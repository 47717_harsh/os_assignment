#!/bin/bash


read -p "Enter path/name of text file: " path


echo "No. of words --->  `cat $path | wc -w`"
echo "No. of characters --->  `cat $path | wc -m`"
echo "No. of white spaces --->  `cat $path | grep -Po "\s" | wc -m`"

echo "No. of special characters ---> `cat $path | grep -Po "[^\d^\w^\s]" | wc -l`"

