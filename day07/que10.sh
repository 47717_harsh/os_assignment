#!/bin/bash


read -p "Enter String: " str

rev=$( echo $str | rev )

if [ $str == $rev ]
then
echo "Given string is palindrome"
else
echo "Given string is not a palindrome"
fi
