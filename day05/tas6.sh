#!/bin/bash

date=$( date | awk -F' ' '{ print $2,$3,$4 }' )

echo "date and time: $date"
echo "username: $( whoami )"
echo "current working directory: $( pwd )"
