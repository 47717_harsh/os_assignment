#!/bin/bash

read -p "enter directory: " dir

if [ -d $dir ]
then
	ls -l "$dir" | grep "^-" | awk -F' ' '{ print $9 }'
else
	echo "Please enter directory"
fi
