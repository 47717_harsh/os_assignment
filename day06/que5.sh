#!/bin/bash

read -p "Enter multiple  process names: " proc

echo $proc > temp.txt

for i in $proc
do
	var=$(pidof $i)
	for j in $var
	do
		kill -9 $j
	done
done
