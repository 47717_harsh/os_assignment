#!/bin/bash

read -p "Enter Process ID: " pid

ps -p $pid | grep -v "TTY" | awk '{ print $NF }'
